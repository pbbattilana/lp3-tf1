#include "header.h"

extern int turno[20];
extern int cont;
extern pthread_mutex_t mutex;
extern pthread_cond_t cond;

int main(int argc, char *argv[])
{
    pthread_t fact_t[20];
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond, NULL);
    char *aux;
    int num;
    for (int i = 1; i < argc; ++i)
    {
        aux = argv[i];
        num = atoi(aux);
        turno[i - 1] = num;
        int *index = malloc(sizeof(int));
        *index = num;
        if (pthread_create(&fact_t[i], NULL, &factfun, index) != 0)
        {
            perror("No se pudo crear el thread");
        }
    }
    for (int i = 1; i < argc; ++i)
    {
        if (pthread_join(fact_t[i], NULL) != 0)
        {
            perror("No se pudo crear el thread");
        }
    }
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);
    return 0;
}