#include "header.h"

int turno[20] = {0};
int cont = 0;
pthread_mutex_t mutex;
pthread_cond_t cond;

void *factfun(void *arg)
{
    while (cont < 20)
    {
        pthread_mutex_lock(&mutex);
        int num = *(int *)arg;
        if (num == turno[cont])
        {
            long fact = 1;
            for (int i = 1; i <= num; i++)
            {
                fact = fact * i;
            }
            printf("Factorial de %d es: %ld \n", num, fact);
            free(arg);
            cont++;
        }
        else
        {
            pthread_cond_wait(&cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&cond);
    }
}